# Generated by Django 4.1.9 on 2023-06-04 05:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todo", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="todo",
            name="done",
            field=models.BooleanField(default=False, verbose_name="task finished"),
        ),
        migrations.AlterField(
            model_name="todo",
            name="due",
            field=models.DateTimeField(),
        ),
    ]
