from django.db import models


class Task(models.Model):
    """a single task"""

    title = models.CharField(max_length=200)
    due = models.DateTimeField()
    done = models.BooleanField("task finished", default=False)
    owner = models.ForeignKey(
        "auth.User",
        related_name="todos",
        on_delete=models.CASCADE,
        default=None,
    )

    def __str__(self):
        return self.title
